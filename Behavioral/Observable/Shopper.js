class Shopper {

    constructor(name) {
        this.name = name;
    }

    notify(name, discount) {
        console.log(`${this.name}, there is a sale at ${name}! ${discount}% of everything`);
    }
}

module.exports = Shopper;
