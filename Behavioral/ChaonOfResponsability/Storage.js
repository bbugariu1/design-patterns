class Storage {
    constructor(name, inventory = [], deliveryTime = 0) {
        this.name = name;
        this.inventory = inventory;
        this.deliveryTime = deliveryTime;

        this.next = null;
    }

    lookInLocalInventory(itemName) {
        const index = this.inventory.map(item => item.name).indexOf(itemName);

        return this.inventory[index];
    }

    setNext(storage) {
        this.next = storage;
    }

    find(name) {
        const found = this.lookInLocalInventory(name);

        if (found) {
            return {
                name: found.name,
                qty: found.qty,
                location: this.name,
                deliveryTime: (this.deliveryTime === 0) ? `now` : `${this.deliveryTime} day()`
            };
        } else if (this.next) {
            return this.next.find(name);
        } else {
            return `we do not carry ${this.item}`;
        }
    }
}

module.exports = Storage;