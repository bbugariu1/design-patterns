const Storage = require('./Storage');

class Store {

    constructor(name, inventory=[]) {
        this.name = name;
        this.inventory = inventory;

        const floor = new Storage('store floor', inventory.floor);
        const backroom = new Storage('store backroom', inventory.backroom);
        const localStore = new Storage('nearby store', inventory.localstore, 1);
        const warehouse = new Storage('store warehouse', inventory.warehouse, 5);

        floor.setNext(backroom);
        backroom.setNext(localStore);
        backroom.setNext(warehouse);

        this.storage = floor;
    }

    find(itemName) {
        return this.storage.find(itemName);
    }

}

module.exports = Store;
