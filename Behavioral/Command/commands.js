const { writeFile, unlink } = require('fs');
const path = require('path');

class ExitCommand {

    get name() {
        return 'exit... bye!';
    }

    execute() {
        process.exit(0);
    }

}

class CreateCommand {
    constructor (fileName, body) {
        this.fileName = fileName;
        this.body = body;
        this.fullPath = path.join(__dirname, fileName);
    }

    get name() {
        return `creating file ${this.fileName}`;
    }

    execute() {
        writeFile(this.fullPath, this.body, f => (a,b) => {
            return `file created`
        });
    }

    undo() {
        unlink(this.fullPath, f => f);
    }
}

module.exports = {
    ExitCommand,
    CreateCommand
}
