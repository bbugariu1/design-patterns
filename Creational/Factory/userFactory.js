const Shopper = require('./Shopper');
const Employee = require('./Employee');

const userFactory = (name, money=0, type, employer) => {
    if (type === "Employee") {
        return new Employee(name, money, employer)
    }

    return new Shopper(name, money)
}

module.exports = userFactory;