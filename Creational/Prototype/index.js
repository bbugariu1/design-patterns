const scoutPrototype = require('./ScoutPrototype');

const alex = scoutPrototype.clone();
const eve = scoutPrototype.clone();

alex.name = "Alex";
eve.name = "Eve";

console.log( `${alex.name}: ${alex.shoppingList}` );
console.log( `${eve.name}: ${eve.shoppingList}` );
