class Logger {

    constructor() {
        this.logs = [];
    }

    get count() {
        return this.logs.length;
    }

    log(message) {
        const timestamp = new Date().toISOString();
        this.logs.push({message, timestamp});
        console.log(`${timestamp} - ${message}`);
    }

}

// class Singleton {
//     static instance = null;
//
//     static getInstance() {
//         if (Singleton.instance === null) {
//             Singleton.instance = new Logger()
//         }
//
//         return Singleton.instance;
//     }
// }

// Sigleton
module.exports = new Logger();
