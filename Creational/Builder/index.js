const Person = require('./Person')
const PersonBuilder = require("./Builder");

// Employees
const sue = new PersonBuilder('Sue')
    .makeEmployee()
    .makeManager(60)
    .build();

const bill = new PersonBuilder('Bill')
    .makeEmployee()
    .makePartTime(20)
    .build();

const phil = new PersonBuilder('Phil')
    .makeEmployee()
    .build();

// Shoppers
const charles = new PersonBuilder('Charles')
    .withMoney(50)
    .withShoppingList(['jeans', 'sunglasses'])
    .build();

const tabbitha = new PersonBuilder('Tabbitha')
    .withMoney(1000)
    .build();


console.log(bill.toString(), charles.toString(), tabbitha.toString());