const CatalogItem = require('./CatalogItem');
const CatalogGroup = require('./CatalogGroup');

const boots = new CatalogItem("Leather Boots", 79.99);
const sneakers = new CatalogItem("Kicks", 39.99);
const flipFlops = new CatalogItem("California work boots", 19.99);

const groupShoes = new CatalogGroup("shoes and such", [boots, sneakers, flipFlops]);
const groupFood = new CatalogGroup("food for love", [
    new CatalogItem('Mango and pepene', 10.99),
    new CatalogItem('Milk with vegan vanila and banana', 13,99)
])
const bag = new CatalogItem('Bag', 0.5);

const fullCatalog = new CatalogGroup(`All products`, [groupShoes, groupFood, bag]);

console.log(`Full catalog total: RON ${fullCatalog.total}`);
fullCatalog.print();
