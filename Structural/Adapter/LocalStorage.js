const {writeFile, existsSync, readFileSync, unlink} = require('fs');

class localStorage {

    constructor() {
        if (existsSync('localStorage.json')) {
            const text = readFileSync('localStorage.json');
            this.items = JSON.parse(text);
        } else {
            this.items = {};
        }
    }

    get length() {
        return Object.keys(this.items).length;
    }

    setItem (key, value) {
        this.items[key] = value;
        writeFile('localStorage.json', JSON.stringify(this.items), error => {
            if (error) {
                console.log(error);
            }
        });
    }

    getItem (key) {
        return this.items[key];
    }

    clear () {
        unlink('localStorage.json', () => {
            console.log(`localStorage.json file has been removed!`);
        });
    }

}

module.exports = new localStorage();