const Shopper = require('./Shopper');
const {
    InventoryItem,
    GoldenInventoryItem,
    DiamondInventoryItem
} = require('./InventoryItem');

const alex = new Shopper('Alex', 5000);

const walkman = new InventoryItem("Walkman", 29.99);
const necklace = new InventoryItem("Necklace", 9.99);

const golderWalkmen = new GoldenInventoryItem(walkman);
const diamondWalkmen = new DiamondInventoryItem(golderWalkmen);

const goldNeklace = new GoldenInventoryItem(necklace);

alex.purchase(goldNeklace);
alex.purchase(diamondWalkmen);

alex.printStatus();
